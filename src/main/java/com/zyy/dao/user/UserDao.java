package com.zyy.dao.user;

import com.zyy.pojo.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @ClassName: UserDao
 * @Description: TODO 类描述
 * @Author: zyy
 * @Date: 2022/01/09 10:38
 * @Version: 1.0
 */
public interface UserDao {
    /**
     * 根据用户编码获取用户信息
     *
     * @param con
     * @param userCode
     * @return
     * @throws SQLException
     */
    User getLoginUser(Connection con, String userCode) throws SQLException;

    /**
     * 修改密码
     *
     * @param con
     * @param id
     * @param password
     * @return
     * @throws SQLException
     */
    int updatePwd(Connection con, int id, String password) throws SQLException;

    /**
     * 根据用户名或者角色查询用户总数
     *
     * @param con
     * @param userName
     * @param userRole
     * @return
     * @throws SQLException
     */
    int getUserCount(Connection con, String userName, int userRole) throws SQLException;

    /**
     * 获取用户列表
     *
     * @param con
     * @param userName
     * @param userRole
     * @param currentPageNo
     * @param pageSize
     * @return
     * @throws SQLException
     */
    List<User> getUserList(Connection con, String userName, int userRole, int currentPageNo, int pageSize) throws SQLException;

    /**
     * 添加用户
     *
     * @param con
     * @param user
     * @return
     * @throws SQLException
     */
    int addUser(Connection con, User user) throws SQLException;

}
