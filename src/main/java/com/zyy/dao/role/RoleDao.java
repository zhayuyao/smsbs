package com.zyy.dao.role;

import com.zyy.pojo.Role;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @InterfaceName: RoleDao
 * @Description: TODO 接口描述
 * @Author: zyy
 * @Date: 2022/02/10 21:10
 * @Version: 1.0
 */
public interface RoleDao {

    /**
     * 获取角色列表
     * @param con
     * @return
     * @throws SQLException
     */
    List<Role> getRoleList(Connection con) throws SQLException;
}
