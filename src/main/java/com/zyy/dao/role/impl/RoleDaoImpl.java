package com.zyy.dao.role.impl;

import com.zyy.dao.BaseDao;
import com.zyy.dao.role.RoleDao;
import com.zyy.pojo.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: RoleDaoImpl
 * @Description: TODO 类描述
 * @Author: zyy
 * @Date: 2022/02/10 21:10
 * @Version: 1.0
 */
public class RoleDaoImpl implements RoleDao {
    @Override
    public List<Role> getRoleList(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Role> roleList = new ArrayList<Role>();
        if (con != null) {
            String sql = "select id, roleName, roleCode from smbms_role";
            Object[] params = {};
            rs = BaseDao.execute(con, ps, rs, sql, params);
            while (rs.next()) {
                Role role = new Role();
                role.setId(rs.getInt("id"));
                role.setRoleCode(rs.getString("roleCode"));
                role.setRoleName(rs.getString("roleName"));
                roleList.add(role);
            }
        }
        return roleList;
    }
}
