package com.zyy.service.user;

import com.zyy.pojo.User;

import java.util.List;

/**
 * @InterfaceName: UserService
 * @Description: TODO 接口描述
 * @Author: zyy
 * @Date: 2022/01/09 11:18
 * @Version: 1.0
 */
public interface UserService {
    /**
     * 登录
     *
     * @param userCode
     * @param password
     * @return
     */
    User login(String userCode, String password);

    /**
     * 获取用户信息根据用户编码
     *
     * @param userCode
     * @return
     */
    User getLoginUser(String userCode);


    /**
     * 修改密码
     *
     * @param id
     * @param password
     * @return
     */
    boolean updatePwd(int id, String password);

    /**
     * 查询记录数
     *
     * @param userName
     * @param userRole
     * @return
     */
    int getUserCount(String userName, int userRole);


    /**
     * 根据条件查询用户列表
     *
     * @param queryUserName
     * @param queryUserRole
     * @param currentPageNo
     * @param pageSize
     * @return
     */
    List<User> getUserList(String queryUserName, int queryUserRole, int currentPageNo, int pageSize);

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    boolean addUser(User user);

}
