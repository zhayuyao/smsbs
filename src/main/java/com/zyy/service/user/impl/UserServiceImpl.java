package com.zyy.service.user.impl;

import com.zyy.dao.BaseDao;
import com.zyy.dao.user.UserDao;
import com.zyy.dao.user.impl.UserDaoImpl;
import com.zyy.pojo.User;
import com.zyy.service.user.UserService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @ClassName: UserServiceImpl
 * @Description: TODO 类描述
 * @Author: zyy
 * @Date: 2022/01/09 11:20
 * @Version: 1.0
 */
public class UserServiceImpl implements UserService {
    /**
     * 业务层会调用dao层，所以这里引入Dao层
     */
    private UserDao userDao;

    public UserServiceImpl() {
        userDao = new UserDaoImpl();
    }

    @Override
    public User login(String userCode, String password) {
        Connection con = null;
        User user = null;
        try {
            con = BaseDao.getConnection();
            user = userDao.getLoginUser(con, userCode);
            if (!password.equals(user.getUserPassword())) {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            BaseDao.closeResource(con);
        }
        return user;
    }

    @Override
    public User getLoginUser(String userCode) {
        Connection con = null;
        User user = null;
        try {
            con = BaseDao.getConnection();
            user = userDao.getLoginUser(con, userCode);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            BaseDao.closeResource(con);
        }
        return user;
    }

    @Override
    public boolean updatePwd(int id, String password) {
        Connection con = null;
        boolean updateFlag = false;
        try {
            con = BaseDao.getConnection();
            int updateCount = userDao.updatePwd(con, id, password);
            if (updateCount > 0) {
                updateFlag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            BaseDao.closeResource(con);
        }
        return updateFlag;
    }

    @Override
    public int getUserCount(String userName, int userRole) {
        Connection con = null;
        int count = 0;
        try {
            con = BaseDao.getConnection();
            count = userDao.getUserCount(con, userName, userRole);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            BaseDao.closeResource(con);
        }
        return count;
    }

    /*@Test
    public void test() {
        UserServiceImpl userService = new UserServiceImpl();
        System.out.println(userService.getUserCount(null, 2));
    }*/

    @Override
    public List<User> getUserList(String queryUserName, int queryUserRole, int currentPageNo, int pageSize) {
        Connection con = null;
        List<User> userList = null;
        try {
            con = BaseDao.getConnection();
            userList = userDao.getUserList(con, queryUserName, queryUserRole, currentPageNo, pageSize);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            BaseDao.closeResource(con);
        }
        return userList;
    }

    /*@Test
    public void test() {
        UserServiceImpl userService = new UserServiceImpl();
        System.out.println(userService.getUserList(null, 0, 1, 5).size());
    }*/

    @Override
    public boolean addUser(User user) {
        Connection con = null;
        boolean addFlag = false;
        try {
            con = BaseDao.getConnection();
            int count = userDao.addUser(con, user);
            if (count == 1) {
                addFlag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            BaseDao.closeResource(con);
        }
        return addFlag;
    }
}
