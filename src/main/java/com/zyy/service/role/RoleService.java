package com.zyy.service.role;

import com.zyy.pojo.Role;

import java.util.List;

/**
 * @InterfaceName: RoleService
 * @Description: TODO 接口描述
 * @Author: zyy
 * @Date: 2022/02/10 21:12
 * @Version: 1.0
 */
public interface RoleService {
    /**
     * 获取角色列表
     * @return
     */
    List<Role> getRoleList();
}
