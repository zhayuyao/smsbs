package com.zyy.service.role.impl;

import com.zyy.dao.BaseDao;
import com.zyy.dao.role.RoleDao;
import com.zyy.dao.role.impl.RoleDaoImpl;
import com.zyy.pojo.Role;
import com.zyy.service.role.RoleService;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @ClassName: RoleServiceImpl
 * @Description: TODO 类描述
 * @Author: zyy
 * @Date: 2022/02/10 21:13
 * @Version: 1.0
 */
public class RoleServiceImpl implements RoleService {
    private RoleDao roleDao;

    public RoleServiceImpl() {
        roleDao = new RoleDaoImpl();
    }

    @Override
    public List<Role> getRoleList() {
        Connection con = null;
        List<Role> roleList = null;
        try {
            con = BaseDao.getConnection();
            roleList = roleDao.getRoleList(con);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            BaseDao.closeResource(con);
        }
        return roleList;
    }

    /*@Test
    public void test() {
        RoleService roleService = new RoleServiceImpl();
        List<Role> roleList = roleService.getRoleList();
        for (Role role : roleList) {
            System.out.println(role.getRoleName());
        }
    }*/
}
