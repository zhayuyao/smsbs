package com.zyy.filter;

import com.zyy.pojo.User;
import com.zyy.util.Constants;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName: SysFilter
 * @Description: TODO 类描述
 * @Author: zyy
 * @Date: 2022/01/09 14:28
 * @Version: 1.0
 */
public class SysFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        User user = (User) req.getSession().getAttribute(Constants.USER_SESSION);
        if (user == null) {
            //已经被移除或者注销了，或者没有登录
            resp.sendRedirect("/smbms/error.jsp");
            return;
        }
        chain.doFilter(req, resp);
    }

    public void destroy() {

    }
}
